Pod::Spec.new do |spec|
  spec.name         = "CCBluetooth"
  spec.version      = "1.0.2"
  spec.summary      = "蓝牙中心管理类"
  spec.description  = <<-DESC
                    Features:
                    1.统一管理蓝牙的状态
                    2.提供协议完成认证过程
                    3.提供协议过滤设备以及连接设备
                   DESC
  spec.homepage     = "https://gitee.com/zhoujinhua_xm"
  spec.license      = "MIT"
  spec.author       = { "周金华" => "zhoujinhua_xm@139.com" }
  spec.ios.deployment_target = '10.0'
  spec.source       = { :git => "https://gitee.com/zhoujinhua_xm/ccbluetooth.git", :tag => spec.version }
  
  spec.source_files = ['Bluetooth/CCBluetooth.{h,m}', 'Bluetooth/CCPeripheral.{h,m}', 'Bluetooth/CCPeripheral.{h,m}', 'Bluetooth/Protocol/*.{h,m}', 'Bluetooth/Command/*.{h,m}']
  spec.exclude_files = ['CCBluetooth', 'CCBluetooth.xcodeproj', 'CCBluetoothTests', 'CCBluetoothUITests']
  

  
  

  
  
end
