Pod::Spec.new do |spec|
  spec.name         = "CCBluetooth"
  spec.version      = "1.0.6"
  spec.summary      = "蓝牙中心管理类"
  spec.description  = <<-DESC
                    Features:
                    1.统一管理蓝牙的状态
                    2.提供协议完成认证过程
                    3.提供协议过滤设备以及连接设备
                   DESC
  spec.homepage     = "https://gitee.com/zhoujinhua_xm"
  spec.license      = "MIT"
  spec.author       = { "周金华" => "zhoujinhua_xm@139.com" }
  spec.ios.deployment_target = '10.0'
  spec.source       = { :git => "https://gitee.com/zhoujinhua_xm/ccbluetooth.git", :tag => spec.version }
  
  spec.source_files = ['Bluetooth/CCBluetooth.{h,m}', 'Bluetooth/CCPeripheral.{h,m}', 'Bluetooth/CCPeripheral.{h,m}']
  
  spec.subspec 'CCProtocol' do |protocol|
    protocol.source_files = ['Bluetooth/CCProtocol/*.{h,m}']
  end
  
  spec.subspec 'CCCommand' do |command|
    command.source_files = ['Bluetooth/CCCommand/*.{h,m}']
  end
  
  
end
