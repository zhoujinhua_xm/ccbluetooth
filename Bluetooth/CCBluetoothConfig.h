//
//  CCBluetoothConfig.h
//  CCBluetooth
//
//  Created by zjh on 2022/1/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCBluetoothConfig : NSObject
+ (void)setLogEnabled:(BOOL)bFlag;
+ (BOOL)logEnabled;
@end

NS_ASSUME_NONNULL_END
