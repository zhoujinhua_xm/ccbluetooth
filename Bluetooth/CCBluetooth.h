//
//  CCBluetooth.h
//  CCBluetooth
//
//  Created by zjh on 2021/8/31.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CCPeripheral.h"
#import "CCAuthProtocol.h"
#import "CCFilterProtocol.h"
NS_ASSUME_NONNULL_BEGIN

/**
 回调状态
 */
typedef NS_ENUM(NSInteger, CCAuthorizationStatus) {
    /// 暂时未处理
    CCAuthorizationStatusDefault        = 0,
    /// 未决定，从未请求过授权
    CCAuthorizationStatusNotDetermined  = 1,
    /// 已授权
    CCAuthorizationStatusAuthorized     = 2,
    
    /// 未配置——在info.plist缺少必要键
    CCAuthorizationStatusNotConfigured  = -1,
    /// 拒绝，用户拒绝授权
    CCAuthorizationStatusDenied         = -2,
    /// 受限制
    CCAuthorizationStatusRestricted     = -3,
    /// 系统不支持
    CCAuthorizationStatusNotSupported   = -4,
    /// 系统设置，需要跳转至系统设置
    CCAuthorizationStatusSystemSetting  = -5,
};

extern NSString *const CCBluetoothPeripheralsChangeNotifyKey;
extern NSString *const CCBluetoothPeripheralConnectedNotifyKey;
extern NSString *const CCBluetoothPeripheralDisconnectedNotifyKey;
/// 为了方便使用，中心管理管理了所有设备的特征值更新
/// 特定业务的蓝牙类抽象，不适用于所有场景
@interface CCBluetooth : NSObject

@property (nonatomic, strong, readonly) NSArray <CCPeripheral *> *peripherals;//内部不排序

@property (nonatomic, strong) id<CCAuthProtocol> authProtocol;//没有认证无需设置

@property (nonatomic, strong) id<CCFilterProtocol> filterProtocol;//没有需求无需设置

@property (nonatomic, assign, readonly) CCAuthorizationStatus authorization; /// 权限

+ (instancetype)share;

+ (void)setLogEnabled:(BOOL)bFlag;

+ (void)interval:(CGFloat)interval;//设置连接超时时间

/// <#Description#>
/// @param queue 设置蓝牙工作的队列-----请注意：
/// ........................优先于第一次初始化对象，不然的话不起作用啊...........................
///
+ (void)registerQueue:(dispatch_queue_t)queue;

/// <#Description#>
/// @param services 设置搜索广播包services
+ (void)registerServices:(NSArray <CBUUID *>*)services;

/// <#Description#>
/// @param delay 开始扫描，delay秒后停止扫描
- (void)startScanAndStopAfterDelay:(NSTimeInterval)delay;

/// 开始扫描
- (void)startScan;

/// 停止扫描
- (void)stopScan;

/// 连接设备
/// @param peripheral <#peripheral description#>
- (void)connect:(CCPeripheral *)peripheral;

/// 断开设备
/// @param peripheral <#peripheral description#>
- (void)disconnect:(CCPeripheral *)peripheral;

/// 断开所有设备
- (void)disconnectAll;

/// 添加代理
/// @param delegate <#delegate description#>
- (void)addDelegate:(id)delegate;

/// 移除代理，只要添加代理的对象释放，自动移除代理
/// @param delegate <#delegate description#>
- (void)removeDelegate:(id)delegate;

@end

NS_ASSUME_NONNULL_END
