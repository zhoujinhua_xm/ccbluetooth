//
//  SoundboxDevice.h
//  AlinkBluetoothSpeechDemo
//
//  Created by Leyteris Lee on 31/10/2017.
//  Copyright © 2017 MTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundboxDevice : NSObject

@property (nonatomic, copy) NSString               *sn;
@property (nonatomic, copy) NSString               *name;
@property (nonatomic, copy) NSString               *mac;
@property (nonatomic, copy) NSString               *uuid;
@property (nonatomic, copy) NSString               *type;
@property (nonatomic, copy) NSString               *category;
@property (nonatomic, copy) NSString               *manufacturer;
@property (nonatomic, copy) NSString               *version;
@property (nonatomic, copy) NSString               *key;
@property (nonatomic, copy) NSString               *secret;
@property (nonatomic, assign) float                currentVolume;


@end
