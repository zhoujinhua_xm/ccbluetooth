//
//  CBPeripheral.h
//  CCBluetooth
//
//  Created by zjh on 2021/8/31.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#include "CCCommand.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CCPeripheralState){
    CCPeripheralStateNone =0,//无
    CCPeripheralStateConnectting = 1,//连接中
    CCPeripheralStateConnected = 2//已连接
};

@interface CCPeripheral : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *mac;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, assign) CCPeripheralState state;
@property (nonatomic, strong) NSMutableArray <CBService *>*services;
@property (nonatomic, strong) NSMutableArray *listenCharacteristices;
@property (nonatomic, assign) NSUInteger maxWriteLength;

+ (void)setLogEnabled:(BOOL)bFlag;

- (id)initWithPeripheral:(CBPeripheral *)peripheral;

- (BOOL)checkDLE;

/// 订阅特征值
/// @param serviceUUID <#serviceUUID description#>
/// @param charactaristicUUID <#charactaristicUUID description#>
- (BOOL)listenForService:(NSString *)serviceUUID characteristic:(NSString *)charactaristicUUID;

/// 移除订阅的特征值
/// @param serviceUUID <#serviceUUID description#>
/// @param charactaristicUUID <#charactaristicUUID description#>
- (BOOL)removeListenForService:(NSString *)serviceUUID characteristic:(NSString *)charactaristicUUID;

/// 移除所有订阅的特征值
- (void)clearListen;

/// 写指令时针对设备写指令
/// @param command <#command description#>
- (void)writeCommand:(CCCommand *)command;
@end

NS_ASSUME_NONNULL_END
