//
//  CCAuthProtocol.h
//  CCBluetooth
//
//  Created by zjh on 2021/8/31.
//

#ifndef CCAuthProtocol_h
#define CCAuthProtocol_h
#import "CCPeripheral.h"

@class CCCommand;

@protocol CCAuthProtocolDelegate <NSObject>

/// 认证结果回传给CCBluetooth,统一处理连接状态，未完成认证统一当作连接失败
/// @param peripheral peripheral description
/// @param success success description
- (void)authResult:(CCPeripheral *)peripheral success:(BOOL)success;

@end

@protocol CCAuthProtocol <NSObject>

@property (nonatomic, weak) id<CCAuthProtocolDelegate> delegate;

/// 认证发起
/// @param peripheral peripheral description
- (void)start:(CCPeripheral *)peripheral;

/// 认证接收设备数据处理
/// @param peripheral <#peripheral description#>
/// @param characteristic <#characteristic description#>
- (void)receive:(CCPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic;

@end

#endif /* CCAuthProtocol_h */
