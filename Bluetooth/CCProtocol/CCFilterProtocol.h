//
//  CCAutoProtocol.h
//  CCBluetooth
//
//  Created by zjh on 2021/9/1.
//

#ifndef CCAutoProtocol_h
#define CCAutoProtocol_h

@class CCPeripheral;

@protocol CCFilterProtocol <NSObject>

/// <#Description#>
/// @param peripheral 实现协议，符合某种规则的设备，过滤设备
- (BOOL)filterDiscover:(CCPeripheral *)peripheral;

/// <#Description#>
/// @param peripheral 实现协议，符合某种规则的设备，搜索到后程序自动调用连接指令
- (BOOL)filterAutoConnect:(CCPeripheral *)peripheral;

@end

#endif /* CCAutoProtocol_h */
