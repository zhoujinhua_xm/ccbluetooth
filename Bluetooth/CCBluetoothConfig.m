//
//  CCBluetoothConfig.m
//  CCBluetooth
//
//  Created by zjh on 2022/1/24.
//

#import "CCBluetoothConfig.h"
#import "CCBluetooth.h"
#import "CCPeripheral.h"
static BOOL logEnabled = NO;
@implementation CCBluetoothConfig
+ (void)setLogEnabled:(BOOL)bFlag {
    logEnabled = bFlag;
    [CCBluetooth setLogEnabled:bFlag];
    [CCPeripheral setLogEnabled:bFlag];
}

+ (BOOL)logEnabled {
    return logEnabled;
}
@end
