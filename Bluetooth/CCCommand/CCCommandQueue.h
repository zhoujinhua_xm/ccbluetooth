//
//  CCCommandQueue.h
//  CCBluetooth
//
//  Created by zjh on 2021/8/31.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CCCommand.h"
NS_ASSUME_NONNULL_BEGIN


/// 指令队列管理
@protocol CCCommandQueueDelegate <NSObject>

- (void)commandQueue:(CCCommand *)command;

@end

@interface CCCommandQueue : NSObject

@property (nonatomic, weak) id<CCCommandQueueDelegate> delegate;

+ (void)interval:(CGFloat)interval;//设置时间间隔

- (void)addCommand:(CCCommand *)command;

@end

NS_ASSUME_NONNULL_END
