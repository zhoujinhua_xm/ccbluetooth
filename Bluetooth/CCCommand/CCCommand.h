//
//  CCCommand.h
//  CCBluetooth
//
//  Created by zjh on 2021/8/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CCCommand : NSObject

//@property (nonatomic, strong) NSString *serviceUUID;

@property (nonatomic, strong) NSString *characteristicUUID;

@property (nonatomic, strong) NSData *data;

@property (nonatomic, assign) BOOL withResponse;

@end

NS_ASSUME_NONNULL_END
