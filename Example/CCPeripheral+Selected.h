//
//  CBPeripheral+Selected.h
//  CCBluetooth
//
//  Created by zjh on 2021/9/1.
//

#import <Foundation/Foundation.h>
#import "CCPeripheral.h"
NS_ASSUME_NONNULL_BEGIN

@interface CCPeripheral (Selected)

@property (nonatomic,assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
