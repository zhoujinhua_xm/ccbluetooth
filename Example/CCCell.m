//
//  CCCell.m
//  CCBluetooth
//
//  Created by zjh on 2021/9/1.
//

#import "CCCell.h"

NSString * const CCCellKey = @"CCCellKey";
@interface CCCell()
@end

@implementation CCCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self __initUI];
    }
    return self;
}
- (void)__initUI {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.titlelb];
    [self.contentView addSubview:self.indicatorView];
    [self.contentView addSubview:self.selectedImgView];
}

- (UILabel *)titlelb {
    if (!_titlelb) {
        _titlelb = [[UILabel alloc] init];
        _titlelb.textColor = UIColor.blackColor;
        _titlelb.frame = CGRectMake(20, 0, ScreenWidth - 90, 50);
    }
    return _titlelb;
}

- (UIActivityIndicatorView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
        _indicatorView.color = UIColor.grayColor;
        _indicatorView.frame = CGRectMake(ScreenWidth - 60, 0, 50, 50);
        _indicatorView.hidesWhenStopped = YES;
    }
    return _indicatorView;
}

- (UIImageView *)selectedImgView {
    if (!_selectedImgView) {
        _selectedImgView = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth - 60, 0, 50, 50)];
        _selectedImgView.contentMode = UIViewContentModeCenter;
        _selectedImgView.image = [UIImage imageNamed:@"selected"];
    }
    return _selectedImgView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
