//
//  CBPeripheral+Selected.m
//  CCBluetooth
//
//  Created by zjh on 2021/9/1.
//

#import "CCPeripheral+Selected.h"
#import <objc/runtime.h>

@implementation CCPeripheral (Selected)

- (void)setSelected:(BOOL)selected{
    objc_setAssociatedObject(self, @selector(selected), @(selected), OBJC_ASSOCIATION_ASSIGN);
}
- (BOOL)selected{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

@end
