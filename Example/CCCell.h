//
//  CCCell.h
//  CCBluetooth
//
//  Created by zjh on 2021/9/1.
//

#import <UIKit/UIKit.h>
extern NSString *const CCCellKey;
NS_ASSUME_NONNULL_BEGIN

@interface CCCell : UITableViewCell
@property (nonatomic, strong) UILabel *titlelb;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIImageView *selectedImgView;
@end

NS_ASSUME_NONNULL_END
