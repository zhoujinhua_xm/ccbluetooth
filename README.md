# CCBluetooth

#### 介绍
关于蓝牙中心管理

#### 软件架构
软件架构说明


#### 安装教程

1.  cocoapods依赖管理podfile配置
    pod 'CCBluetooth' source 'https://gitee.com/zhoujinhua_xm/CCBluetooth.git'
    

#### 使用说明

1.  注册搜索特定广播的uuid
    CCBluetooth.registerServices([CBUUID(string: "0xFFF0")])    
    
2.  注册线程
    CCBluetooth.register(DispatchQueue.global())
    
    开始扫描
    CCBluetooth.share().startScan()

    结束扫描
    CCBluetooth.share().stopScan()

    设备变化的通知    CCBluetoothPeripheralsChangeNotifyKey
    设备连接的通知    CCBluetoothPeripheralConnectedNotifyKey
    设备断开的通知    CCBluetoothPeripheralDisconnectedNotifyKey

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
